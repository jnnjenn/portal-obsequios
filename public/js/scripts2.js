$(document).ready(function(){
	Recarga();
});

function Recarga(){
    var tablaDatos = $("#datos");
    var route      = "http://localhost:8000/categories";
	$("#datos").empty();

    $.get(route, function (res){
        $(res).each(function(key, value){
            tablaDatos.append("<tr><td>"+ value.title_en +"</td><td>"+ value.title_fr +"</td><td><button class='btn btn-primary' OnClick='mostrar("+ value.id +")' data-toggle='modal' data-target='#myModal'>Edit</button><button class='btn btn-danger' OnClick='eliminar("+ value.id +")'>Delete</button></td></tr>");
        });
    });
};

function mostrar(id){    
    var route = "http://localhost:8000/category/"+ id + "/edit";
    $.get(route, function(res){
		console.clear();
		console.log(res);

        $("#ctg_title_en").val(res.title_en);
        $("#ctg_title_fr").val(res.title_fr);
        $("#id").val(res.id); 
    });
}


$("#actualizar").click(function(){
	var value    = $("#id").val();
	var title_en = $("#ctg_title_en").val();
    var title_fr = $("#ctg_title_fr").val();
	var route    = "http://localhost:8000/category/"+value+"";
	var token    = $("#token").val();

	$.ajax({
		url     : route,
		headers : {'X-CSRF-TOKEN': token},
		type    : 'PUT',
		dataType: 'json',
		data    : { title_en: title_en, title_fr: title_fr },
		success : function(){
			Recarga();
			$("#myModal").modal('toggle');
			$("#msj-success-modified").fadeIn();
		},
        error:function(msj){
            $("#msj").html(msj.responseJSON.title_en + " <br> " + msj.responseJSON.title_fr);
            $("#msj-error").fadeIn();            
        }
	});
});


function eliminar(id){
	var route = "http://localhost:8000/category/"+ id;
	var token = $("#token").val();

	$.ajax({
		url     : route,
		headers : {'X-CSRF-TOKEN': token},
		type    : 'DELETE',
		dataType: 'json',
		success : function(){
			Recarga();
			$("#msj-success-delete").fadeIn();
		}
	});
}