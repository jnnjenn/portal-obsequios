$("#ctg_register").click(function(){
    var title_en = $("#ctg_title_en").val();
    var title_fr = $("#ctg_title_fr").val();
    var token    = $("#token").val();
    var route    = "http://localhost:8000/category";

    $.ajax({
        url     : route,
        headers : {'X-CSRF-TOKEN': token },
        type    : 'POST',
        dataType: 'json',
        data    : {title_en: title_en, title_fr: title_fr },
        success:function(){
            $("#msj-success").fadeIn();
        },
        error:function(msj){
            $("#msj").html(msj.responseJSON.title_en + " <br> " + msj.responseJSON.title_fr);
            $("#msj-error").fadeIn();            
        }
    });
});