<?php
namespace portal\Http\Requests;
use portal\Http\Requests\Request;

class ProductCreateRequest extends Request{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'title_en'       => 'required',
            'title_fr'       => 'required',
            'description_en' => 'required',
            'description_fr' => 'required',
            'value'          => 'required'
        ];
    }
}
