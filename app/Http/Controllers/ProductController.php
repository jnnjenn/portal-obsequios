<?php

namespace portal\Http\Controllers;

use portal\Http\Requests;
use portal\Http\Requests\ProductCreateRequest;
use portal\Http\Requests\ProductUpdateRequest;
use portal\Http\Controllers\Controller;

use portal\Category;
use portal\Product;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ProductController extends Controller{
    
    public function __construct(){
		$this->middleware('auth');
		$this->beforeFilter('@find', ['only' => ['edit', 'update', 'destroy']]);
	}

    public function find(Route $route){
		$this->product = Product::find($route->getParameter('product'));
	}

    public function index(){
        $products = Product::paginate(2);
		return view('product.index', compact('products'));
    }

    public function create(){
        $categories = Category::lists('title_en', 'id');
        return view('product.create', compact('categories'));
    }

    public function store(ProductRequest $request){
        Product::create($request->all());
		Session::flash('message', 'Success!! Product created correctly.');
		return Redirect::to('/product');
    }


    public function edit($id){
        return view('product.edit', ['product'=>$this->product]);
    }

    public function update(Request $request, $id){
        $this->product->fill($request->all());
		$this->product->save();

		Session::flash('message', 'Success!! Product updated correctly.');
		return Redirect::to('/product');
    }

    public function destroy($id){
        $this->product->delete();
		Session::flash('message', 'Success!! Product deleted correctly.');
		return Redirect::to('/product');
    }
}
