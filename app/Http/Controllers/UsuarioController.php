<?php 
namespace portal\Http\Controllers;

use portal\Http\Requests;
use portal\Http\Requests\UserCreateRequest;
use portal\Http\Requests\UserUpdateRequest;
use portal\Http\Controllers\Controller;

use portal\User;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;


class UsuarioController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('admin', ['only' => ['create', 'edit'] ]);
		$this->beforeFilter('@find', ['only' => ['edit', 'update', 'destroy']]);
	}

	public function find(Route $route){
		$this->user = User::find($route->getParameter('usuario'));
	}


	public function index(){	
		$users = User::paginate(10);
		return view('usuario.index', compact('users'));
	}

	public function create(){
		return view('usuario.create');
	}

	public function store(UserCreateRequest $request){
		User::create($request->all());
		Session::flash('message', 'Success!! User created correctly.');
		return Redirect::to('/usuario');
	}

	public function show($id){
		
	}

	public function edit($id){
		return view('usuario.edit', ['user'=>$this->user]);
	}

	public function update($id, UserUpdateRequest $request){
		$this->user->fill($request->all());
		$this->user->save();

		Session::flash('message', 'Success!! User updated correctly.');
		return Redirect::to('/usuario');
	}

	public function destroy($id){
		$this->user->delete();
		Session::flash('message', 'Success!! User deleted correctly.');
		return Redirect::to('/usuario');
	}

}
