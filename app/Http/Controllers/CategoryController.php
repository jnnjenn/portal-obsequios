<?php
namespace portal\Http\Controllers;

use portal\Http\Requests;
use portal\Http\Requests\CategoryRequest;
use portal\Http\Controllers\Controller;

use portal\Category;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class CategoryController extends Controller{
    
    public function __construct(){
        $this->beforeFilter('@find', ['only' => ['edit', 'update', 'destroy']]);
	}

    public function find(Route $route){
		$this->category = Category::find($route->getParameter('category'));
	}

    public function listing(){
        $categories = Category::all();
        return response()->json(
            $categories->toArray()
        );
    }

    public function index(){
		return view('category.index');
    }

    public function create(){
        return view('category.create');
    }

    public function store(CategoryRequest $request){
        if($request->ajax()){
            Category::create($request->all());
            return response()->json([
                "mensaje" => 'Category created successful.'
            ]);
        } 
    }

    public function edit($id){
        return response()->json(
            $this->category->toArray()
        );
    }

    public function update(CategoryRequest $request, $id){
        $this->category->fill($request->all());
        $this->category->save();
        return response()->json(["mensaje" => 'Category modified successful.']);  
    }

    public function destroy($id){
        $this->category->delete();
        return response()->json(["mensaje" => 'Category deleted successful.']);
    }

}
