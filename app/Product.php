<?php
namespace portal;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Product extends Model{
    use SoftDeletes;

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title_en', 'title_fr', 'description_en', 'description_fr', 'value', 'category_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $dates  = ['deleted_at'];

	public function setPathAttribute($path){
		$this->attributes['path'] = Carbon::nomw()->secong.$path->getClientOriginalName();
		$name = Carbon::nomw()->secong.$path->getClientOriginalName();
		\Storage::disk('local')->put( $name, \File::get($path) );
	}

}
