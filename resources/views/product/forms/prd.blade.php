<div class="form-group">
    {!!Form::label("Product's Title in English:")!!}
    {!!Form::text('title_en', null, ['id'=>'ctg_title_en', 'class'=>'form-control', 'placeholder'=>"Enter product's title in English"])!!}
</div>

<div class="form-group">
    {!!Form::label("Product's Title in French:")!!}
    {!!Form::text('title_fr', null, ['id'=>'ctg_title_fr', 'class'=>'form-control', 'placeholder'=>"Enter product's title in French"])!!}
</div>

<div class="form-group">
    {!!Form::label("Product's Description in English:")!!}
    {!!Form::text('description_en', null, ['id'=>'ctg_description_en', 'class'=>'form-control', 'placeholder'=>"Enter product's description in English"])!!}
</div>

<div class="form-group">
    {!!Form::label("Product's Description in French:")!!}
    {!!Form::text('description_fr', null, ['id'=>'ctg_description_fr', 'class'=>'form-control', 'placeholder'=>"Enter product's descriptionme in French"])!!}
</div>

<div class="form-group">
	{!!Form::label('Category','Category:')!!}
	{!!Form::select('category_id', $categories)!!}
</div>

<div class="form-group">
	{!!Form::label('Image','Image:')!!}
	{!!Form::file('path')!!}
</div>