@extends('layouts.admin')
@section('content')

        {!!Form::open(['route'=>'product.store', 'method'=>'POST', 'files' => true])!!}
            @include('alerts.request')
            @include('product.forms.prd')
            {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
        {!!Form::close()!!}    
    @endsection

@section('scripts')
    {!!Html::script('js/scripts.js')!!}
@endsection