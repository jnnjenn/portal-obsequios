@extends('layouts.admin')
@section('content')

    @include('alerts.request')
    @include('alerts.errors')

    {!!Form::model($user, ['route'=> ['usuario.update', $user->id], 'method'=>'PUT'])!!}
        @include('usuario.forms.usr')
        {!!Form::submit('Save',['class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

    {!!Form::open( ['route'=> ['usuario.destroy', $user->id], 'method'=>'DELETE'])!!}
        {!!Form::submit('Delete',['class'=>'btn btn-danger'])!!}
    {!!Form::close()!!}

@endsection