@extends('layouts.admin')
@section('content')
    
    @include('alerts.request')
    @include('alerts.errors')

    {!!Form::open(['route'=>'usuario.store', 'method'=>'POST'])!!}
        @include('usuario.forms.usr')
        {!!Form::submit('Save',['class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
    
@endsection