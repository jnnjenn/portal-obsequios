@extends('layouts.admin')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    @include('alerts.success')
                    @include('alerts.errors')

                    <table class="table table-bordered">
                        <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </thead>
                        @foreach($users as $user)
                            <tbody>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    {!!link_to_route('usuario.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class' => 'btn btn-info'])!!}
                                </td>
                            </tbody>
                        @endforeach
                    </table>

                    {!!$users->render()!!}
                </div>
            </div>
        </div>
    </div>
@endsection
