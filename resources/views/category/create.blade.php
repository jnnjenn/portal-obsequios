@extends('layouts.admin')
@section('content')
    
    {!!Form::open(['route'=>'category.store', 'method'=>'POST'])!!}
        <div id="msj-success" class="alert alert-info alert-dismissible" role="alert" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <b>Category create successful.</b>
        </div>

        <div id="msj-error" class="alert alert-danger alert-dismissible" role="alert" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <b id="msj"></b>
        </div>
        
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" >
        @include('category.forms.ctg')
        {!!link_to('#', $title='Create', $attributes = ['id'=>'ctg_register', 'class'=>'btn btn-primary'], $secure = null)!!}
    {!!Form::close()!!}
    
@endsection

@section('scripts')
    {!!Html::script('js/scripts.js')!!}
@endsection