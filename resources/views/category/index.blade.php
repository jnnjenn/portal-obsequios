@extends('layouts.admin')
@section('content')
@include('category.modal')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <div id="msj-success-modified" class="alert alert-info alert-dismissible" role="alert" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <b>Category modified successful.</b>
                    </div>
                    <div id="msj-success-delete" class="alert alert-info alert-dismissible" role="alert" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <b>Category deleted successful.</b>
                    </div>

                    <table class="table table-bordered">
                        <thead>
                            <th>Title in English</th>
                            <th>Title in French</th>
                            <th>Actions</th>
                        </thead>
                        <tbody id="datos"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {!!Html::script('js/scripts2.js')!!}
    {!!Html::script('js/scripts3.js')!!}
@endsection
