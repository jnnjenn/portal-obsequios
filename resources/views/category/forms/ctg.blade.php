<div class="form-group">
    {!!Form::label('Category in English:')!!}
    {!!Form::text('title_en', null, ['id'=>'ctg_title_en', 'class'=>'form-control', 'placeholder'=>'Enter Category Name in English'])!!}
</div>

<div class="form-group">
    {!!Form::label('Category in French:')!!}
    {!!Form::text('title_fr', null, ['id'=>'ctg_title_fr', 'class'=>'form-control', 'placeholder'=>"Enter Category's Name in French"])!!}
</div>