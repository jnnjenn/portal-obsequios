<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#"><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/usuario/create">Create</a></li>
                    <li><a href="/usuario">List</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><span>Categories</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/category/create">Create</a></li>
                    <li><a href="/category">List</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><span>Products</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/product/create">Create</a></li>
                    <li><a href="/product">List</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>