<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Portal Obsequios</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        {!!Html::style('css/bootstrap-responsive.css')!!}
        {!!Html::style('css/style.css')!!}
        {!!Html::style('color/default.css')!!}
    </head>
    <body>
        <div class="navbar-wrapper">
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </a>
                        <h1 class="brand"><a href="index.html">Portal de Obsequios</a></h1>
                        <!-- navigation -->
                        <nav class="pull-right nav-collapse collapse">
                        <ul id="menu-main" class="nav">
                            <li><a title="/" href="#about">Home</a></li>
                            <li><a title="services" href="#services">Categories</a></li>
                            <li><a title="works" href="#works">Products</a></li>
                            <li><a href="/admin">Adminis</a></li>
                        </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>


				<section class="content">
                    @yield('content')
                </section>


	@include('adminfooter')

    {!!Html::script('js/jquery.js')!!}
    {!!Html::script('js/jquery.scrollTo.js')!!}
    {!!Html::script('js/jquery.nav.js')!!}
    {!!Html::script('js/jquery.localscroll-1.2.7-min.js')!!}
    {!!Html::script('js/jquery.prettyPhoto.js')!!}
    {!!Html::script('js/js/custom.js')!!}
</body>
</html>


                