<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Portal Obsequios</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {!!Html::style('css/bootstrap_admin.min.css')!!}   
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        {!!Html::style('css/AdminLTE.min.css')!!}
        {!!Html::style('css/skins/skin-purple-light.min.css')!!}
    </head>
    <body class="skin-purple-light">
        <div class="wrapper">

            <!-- Header -->
            @include('adminheader')

            <!-- Sidebar -->
            @include('adminsidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content">
                    @yield('content')
                </section>
            </div>

            <!-- Footer -->
            @include('adminfooter')

        </div>

        {!!Html::script('js/jquery.js')!!}
        {!!Html::script('js/jQuery/jQuery-2.2.3.min.js')!!}
        {!!Html::script('js/jquery.magnific-popup.js')!!}
        {!!Html::script('js/bootstrap.min.js')!!}
        {!!Html::script('js/app.min.js')!!}

        @section('scripts')
        @show
    </body>
</html>