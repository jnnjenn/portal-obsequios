@extends('layouts.principal')
	@section('content')
		<section id="contact" class="section">
			<div class="container">
				<h2>Login</h2>
				<div class="row">
					<div class="span6">
						<div class="cform" id="contact-form">
							@include('alerts.errors')
							@include('alerts.request')
							
							{!!Form::open(['route'=>'log.store', 'method'=>'POST'] )!!}
								<div class="row">
									<div class="span6">
										<div class="form-group">
											{!!Form::label('email','Email:')!!}	
											{!!Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Enter your email'])!!}
										</div>										
										<div class="form-group">
											{!!Form::label('password','Password:')!!}	
											{!!Form::password('password',['class'=>'form-control', 'placeholder'=>'Enter your password'])!!}
										</div>
									</div>
									<div class="span6">
										{!!Form::submit('Login',['class'=>'btn btn-theme pull-left'])!!}
									</div>
								</div>
							{!!Form::close()!!}
						</div>
					</div>
				</div>
			</div>
		</section>
	@endsection	